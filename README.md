# bloc
> The smallest ARM Cortex M-series breakout board available

  ![](http://www.mullac.eu/wp-content/uploads/2016/10/Bloc.png)

### Table of Contents

  1. Specifications
  1. How to install
    1. Linux
    1. Windows
    1. OSX
    1. IDEs
      1. Arduino IDE
  1. How to use
    1. Bootmodes
    1. Tooling
  1. Licensing

## Specifications

  * Dimensions 32.5mm x 21mm x 5mm without headers
  * 4.5 grams with headers
  * ATSAMD21G18 @ 48MHz with 3.3V logic/power
  * 256KB of eMMC memory + 32KB of RAM
  * 32.768 KHz crystal for clock generation & RTC
  * 3.3V regulator with 500mA peak current output
  * USB native support, comes with USB bootloader and serial port debugging
  * 16 GPIO pins
  * Hardware Serial, hardware I2C, hardware SPI, hardware SWD support
  * PWM outputs on all pins
  * 6 x 12-bit analog inputs
  * 1 x 10-bit analog output (DAC)
  * Pin #13 blue LED for general purpose blinking
  * Power indication LED
  * Reset button

## How to install

### Linux

There are no additional drivers needed in order to work with the `bloc`
on Linux. In order to program a `bloc` your user will have to be added
to the `dialout`-group. You can do this by running the following command:

    sudo usermod -a -G dialout username

You should be able to program the `bloc` after logging out and then
logging back in.

### Windows

In order to work with the `bloc` on Windows you will have to install
the [Adafruit drivers](https://github.com/adafruit/Adafruit_Windows_Drivers/releases/download/1.0.0.0/adafruit_drivers.exe).

### OSX

There are no additional drivers needed in order to work with the `bloc`
on OSX.

### IDEs

#### Arduino IDE

It is possible to use the `bloc` with the Arduino IDE. Make sure you
have the latest version of the [Arduino IDE](https://www.arduino.cc/en/Main/Software) installed on your system.

1. In the Arduino IDE, go to **File** *->* **Preferences**.

 ![alt text](https://cdn-learn.adafruit.com/assets/assets/000/025/279/original/flora_Screen_Shot_2015-05-07_at_9.04.49_AM.png?1431003909)

1. Copy and Paste the following link in the **Additional Boards Manager URLs**:  https://adafruit.github.io/arduino-board-index/package_adafruit_index.json

 ![alt text](https://cdn-learn.adafruit.com/assets/assets/000/025/281/medium640/flora_Screen_Shot_2015-05-07_at_9.07.21_AM.png?1431004060)

1. In order to support more than one Boards Manager URL separate them by a comma.

1. Click **OK** in order to save the Boards Manager.

1. Go to **Tools** *->* **Board: <?>** *->* **Boards Manager**.

  ![alt text](https://cdn-learn.adafruit.com/assets/assets/000/028/791/medium800/adafruit_products_boardmanager.png?1448652571)

1. Install **Arduino SAMD Boards (32-bits ARM Cortex-M0+)** version 1.6.2 (or later).

 ![](https://cdn-learn.adafruit.com/assets/assets/000/028/792/medium800/adafruit_products_arduinosamd162.png?1448652786)

1. Install **Adafruit SAMD Boards**.

  ![](https://cdn-learn.adafruit.com/assets/assets/000/028/794/medium800/adafruit_products_adafruitsamd.png?1448652973)

1. Restart the Arduino IDE in order to load the new boards. You should now be able to program the `bloc` by going to **Tools** *->* **Board** and selecting **Adafruit Feather M0 (Native USB Port)**.

  ![](https://cdn-learn.adafruit.com/assets/assets/000/028/795/medium800/adafruit_products_boardm0.png?1448653613)

## How to use

### Bootmodes

Before programming the `bloc`, double tap the reset button to go into
flash mode. While in flash mode the LED next to the reset button will
fade in and out slowly.

### Tooling

  1. Flashing utilities
    * [BOSSA](https://github.com/shumatech/BOSSA)
    * [armdude](#)
  1. Bootloaders
    * [SAM-BA](http://www.atmel.com/tools/ATMELSAM-BAIN-SYSTEMPROGRAMMER.aspx)

## Licensing

The `bloc` is licensed under the [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/). You should have
received a copy of the license with this document (see `LICENSE`).

![](https://upload.wikimedia.org/wikipedia/commons/6/62/CC-BY-SA-Andere_Wikis_%28v%29.svg)

For more information contact us [through email](info@mullac.eu).
